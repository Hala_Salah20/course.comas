<?php

namespace App\Models;

use CodeIgniter;
use CodeIgniter\Model;

class CategoriesModel extends Model
{
/**
 * Undocumented function
 */
    public function __construct()
    {
        $this->db = \Config\DataBase::connect();

        $this->builder = $this->db->table('categories');
    }
    public function getCategories()
    {
        $this->builder->select('id, name, description, language_code, category_image');

        return $this->builder->get()->getResultArray();
    }
    public function getCategory($id)
    {
        $this->builder->select('id, name, description, category_image');
        $this->builder->where('id', $id);
        return $this->builder->get()->getRowArray();
    }

    public function addCategory($data)
    {
        $this->builder->insert($data);
    }

    public function editCategory($id, $data)
    {
        $this->builder->where('id', $id);
        $this->builder->update($data);
    }

    public function deleteCategory($id)
    {
        $this->builder->where('id', $id);
        $this->builder->delete();
    }
}
