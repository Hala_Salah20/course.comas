<?php namespace App\Models;

use CodeIgniter\Model;

class CoursesModel extends Model
{
     public function __construct()
    {
        $this->db = \Config\DataBase::connect();
        $this->builder = $this->db->table('courses'); 
        $this->videoBuilder = $this->db->table('videos');
 
    }

    public function getCategories1()
    {
        $this->builder = $this->db->table('categories');
        $this->builder->select('name, id');
        return $this->builder->get()->getResultArray();
    }

   public function getCourses()
   {
    $this->builder = $this->db->table('courses');
    $this->builder->select('course_image , courses.id , courses.name, courses.description, courses.language_code, categories.name as category_name');
    $this->builder->join('categories', 'courses.category_id = categories.id');
    return $this->builder->get()->getResultArray();
}

   public function getCourse($id){

    $this->builder->select('id , name, description, language_code, category_id , course_image');
    $this->builder->where('id',$id);
    return $this->builder->get()->getRowArray();

    }
   public function insertCourse($data)
   {
       
       $added = $this->builder->insert($data);
    }

    public function getOneCourse($name){
        $this->builder->select('id , name');
        //select id 
        $this->builder->where('name',$name);
        //return row 
        return $this->builder->get()->getRowArray();
    }

   public function editCourse($id,$data){
        
        $this->builder->where('id',$id);
        $this->builder->update($data);
    }

    
   public function deleteCourse($id)
   {
<<<<<<< HEAD
        $this->builder->where('id', $id);
=======
<<<<<<< HEAD
        $this->builder->where('id', $id);
=======
<<<<<<< HEAD
        $this->builder->where('id', $id);
=======
<<<<<<< HEAD
        $this->builder->where('id', $id);
=======
<<<<<<< HEAD
        $this->builder->where('id', $id);
=======
       $this->builder->where('id', $id);
>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
        $this->videoBuilder->where('course_id', $id);
        $this->builder->delete();
        $this->videoBuilder->delete();
    }
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0

    public function categoryCourses($id)
    {
        $this->builder->where('category_id', $id);
        return $this->builder->get()->getResultArray();
    }
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
=======
>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
}