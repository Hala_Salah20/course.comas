<?php

namespace App\Models;

use CodeIgniter\Model;

class VideosModel extends Model
{
    public function __construct()
    {
        //contect with data base
        $this->db = \Config\DataBase::connect();
        //select table i want to show in page
        $this->builder = $this->db->table('videos');

    }


    public function getCourses()
    {
        $this->builder = $this->db->table('courses');
        $this->builder->select('name, id');
        return $this->builder->get()->getResultArray();
    }

    public function getVideos()
    {
        $this->builder = $this->db->table('videos');
        $this->builder->select('videos.id, videos.title, courses.name as course_name');
        $this->builder->join('courses', 'courses.id = videos.course_id');
        return $this->builder->get()->getResultArray();
    }

    public function insertvideo($data)
    {
        $this->builder->insert($data);
    }

    public function editVideo($id, $data)
    {
        $this->builder->where('id',$id);
        $this->builder->update($data);
    }

    public function getVideo($id)
    {
        $this->builder->select('id, title, course_id');
        $this->builder->where('id', $id);
        return $this->builder->get()->getRowArray();
    }

    public function deleteVideo($id)
    {
        $this->builder->where('id', $id);
        $this->builder->delete();
    }
}
