<?php namespace App\Models;
use CodeIgniter\Model;

class EnrollmentsModel extends Model
{
    public function __construct()
    {
        //contect with data base
        $this->db = \Config\DataBase::connect();
        //select table i want to show in page
        $this->builder = $this->db->table('enrollments');
    }

}