<?php namespace App\Models;

use CodeIgniter\Model;

class ContactUsModel extends Model{

    public function __construct()
    {
        $this->db = \Config\DataBase::connect();

        $this->builder = $this->db->table('contactUs');
    }
    public function getContacts()
    {
        $this->builder->select('id,icon_code,title,link');

        return $this->builder->get()->getResultArray();
    }
    public function getContact($id)
    {
        $this->builder->select('id ,icon_code , title ,link');
        $this->builder->where('id', $id);
        return $this->builder->get()->getRowArray();
    }
    public function addContacts($data)
    {
        $this->builder->insert($data);
 
    }
    public function editContacts($id,$data)
    {
        $this->builder->where('id', $id);
        $this->builder->update($data);
    }

    public function deleteContacts($id)
    {
        $this->builder->where('id', $id);
        $this->builder->delete('');
    }
}




