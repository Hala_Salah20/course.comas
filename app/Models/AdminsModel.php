<?php namespace App\Models;
use CodeIgniter\Model;

class AdminsModel extends Model
{
    public function __construct()
    {
        //contect with data base
        $this->db = \Config\DataBase::connect();
        //select table i want to show in page
        $this->builder = $this->db->table('admins');
    }

    public function getAdmins ()
    {
       //select column i want to see in page
       $this->builder->select('id , email , user_name , is_active');
       //send this columns as array
       return $this->builder->get()->getResultArray();
    }

    public function saveAdmin ($data)
    {
        //insert data in dataBase
        $this->builder->insert($data);
    }

    public function getAdmin ($id)
    {
        //i want to show some of rows in edit page so use select
        $this->builder->select('id , email , user_name , password , is_active ');
        //select id 
        $this->builder->where('id',$id);
        //return row 
        return $this->builder->get()->getRowArray();
    }

    public function editAdmin($id,$data)
    {
        $this->builder->where('id',$id);
        $this->builder->update($data);
    }

    public function deleteAdmin($id)
    {
        $this->builder->where('id', $id);
        $this->builder->delete();
    }
}
