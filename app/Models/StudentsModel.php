<?php namespace App\Models;

use CodeIgniter\Model;
use Config\Email;

class StudentsModel extends Model{

    public function __construct(){
        
        $this->db = \Config\DataBase::connect();
        $this->builder = $this->db->table('students');
        
    }


    function getStudent()
    {
        $this->builder->select();
        return  $this->builder->get()->getResultArray();
    }

    function addStudent($data)
    {
  
        $this->db->table('students')->insert($data);
    } 

    function singleData($id)
    {
        $this->builder->where('id',$id);
       return   $this->builder->get()->getRowArray();
    } 

    function singleStudent($email)
    {
       $this->builder->where('email',$email);
       return   $this->builder->get()->getRowArray();

    } 



    function updateStudent($id,$data)
    {
        $this->builder->where('id',$id);
        $this->builder->update($data);
    } 

    function deleteStudent($id)
    {
        $this->builder->where('id',$id)->delete();
    } 
    
}
?>