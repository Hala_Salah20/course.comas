<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Welcome to CodeIgniter 4!</title>
	<meta name="description" content="The small framework with powerful features">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>"/>

	<link href="<?= base_url('assets/css/home.min.css'); ?>" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
</head>
<body>
<div class ='header-background'>
    <div id= 'nav' class ='sticky-nav'>
        <nav class="navbar navbar-expand-lg">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                       <a><li class="nav-item"><a class="nav-link" href="#">Category</a></li>
                       <a><li class="nav-item"><a class="nav-link" href="#">Search</a></li>
                       <a><li class="nav-item"><a class="nav-link" href="#">Language</a></li>
                    </ul>
                </div>
                <div class="d-flex align-items-center">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                       <a><li class="nav-item"><a class="nav-link" href="#">Login</a></li>
                       <a><li class="nav-item"><a class="nav-link" href="#">Register</a></li>
                        <a><li class="nav-item"><a class="nav-link" href="#">About us</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
</body>
</html>
