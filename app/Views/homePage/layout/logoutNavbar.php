<nav class="navbar navbar-expand-lg navigation-wrap">
    <div class="container">
        <a class="navbar-brand" href="/Homepage/Home"><img src="<?php echo base_url('assets/images/logo.png') ?>" class="homeLogo"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-stream navbar-toggler-icon"></i>

        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item"> <a class="nav-link" aria-current="page" href="<?php echo base_url('homepage/studentHomepage') ?>">Home</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() . '/HomePage/Categories' ?>">Category</a></li>
                <li class="nav-item"> <a class="nav-link" href="#about">About Us</a></li>
                <li class="nav-item"> <a class="nav-link" href="#contact">Contact Us</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo base_url() . '/HomePage/Login/logout' ?>">Log out</a></li>
            </ul>
        </div>
    </div>
</nav>