<nav class="navbar navbar-expand-lg navigation-wrap">
        <div class="container">
        <?php 
         use App\Models\SiteInfoModel;
         $siteInfo = new SiteInfoModel ();
         $data= $siteInfo->getsiteInfos();
        if(!empty($data)){
          foreach($data as $row){?>
            <a class="navbar-brand" href="#">  <img src="/uploads/<?php echo $row['logo_path']?>" class="homeLogo"></a>
           <?php
          }
        }?>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" 
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-stream navbar-toggler-icon"></i>

            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item"> <a class="nav-link" aria-current="page" href= "<?php echo base_url().'/Home'?>">Home</a></li>
                    <li class="nav-item"> <a class="nav-link" href="#category">Category</a></li>
                    <li class="nav-item"> <a class="nav-link" href="#about" >About Us</a></li>
                    <li class="nav-item"> <a class="nav-link" href="#contact" >Contact Us</a></li>
                    <li class="nav-item"> <a class="nav-link" href="<?php echo base_url().'/homepage/login'?>">Log in</a></li>
                    
                </ul>
            </div>
        </div>
    </nav>