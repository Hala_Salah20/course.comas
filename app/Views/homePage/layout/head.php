<head>



<?php
use App\Models\SiteInfoModel;
$siteInfo = new SiteInfoModel ();
 $data= $siteInfo->getsiteInfos();
 if(!empty($data)){
 foreach($data as $row){?>
  <meta charset="UTF-8">
	<title><?php echo $row['name']; ?></title>
  <link rel="shortcut icon" type="image/png" href="/uploads/<?php echo $row['icon_path']?>"/>
            <?php
            } }?>
  <meta name="description" content="The small framework with powerful features">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link href="<?= base_url('assets/css/publicPage.min.css'); ?>" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
