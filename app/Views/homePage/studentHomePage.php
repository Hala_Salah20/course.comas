<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Course Com</title>
	<meta name="description" content="The small framework with powerful features">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<link href="<?= base_url('assets/css/studentHomePage.min.css'); ?>" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>

</head>

<body>
<!-- HEADER -->
<header>
<?= view('homePage/layout/logoutNavbar'); ?>

</header>
<section id="home">
  <div class="container-fluid px-0 top-banner">
    <div class="container" >
      <div class="row">
        <div class="col-lg-5 col-md-6">
          <h2>Whatever you want to do,<br> this is the place to start learning how to do it.</h2>
          
          <div class="mt-4">
            <a href="<?php echo base_url().'/homepage/signup'?>">
            
            </a>
          </div>
        </div>
        <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
          <img src="<?php echo base_url('assets/images/studentHomePage.jpg')?>" class="img-fluid" alt="" >
        </div>
      </div>
    </div>
  </div>
</section>

<!-------------about section start ---------->
<section id="about">
  <div class="container-fluid" id="aboutus">
    <div class="row">
      <div class ="col-xs-12 col-sm-4 col-lg-4 column-left">
        <img src="<?php echo base_url('assets/images/aboutUs.jpg')?>" class="about-us-photo">
      </div>
      <div class ="col-xs-12 col-sm-5 col-lg-5 column-right">
        <h1 class ="section-title">About Us</h1>
        <h4>We share knowledge with the world</h4>
        <h6>On the coursecom site, you can access the following:</h6>
        <p class="paragraph" >
        
       
        <br>1- Self-guided online courses<br>
        <br>2- Courses in all fields and specializations<br>
        <br>3- Arabic language courses<br>
        <br>
        At Coursecom, we are all learners and educators. We live our values every day to create a culture that is diverse, inclusive and committed to helping employees thrive.<br>
        <br>
        We are committed to changing the future of learning for the better.
        <br>
      </p>
    </div>
  </div>
</div>
</section>
<!-------------about section end ---------->
<!-- footer section starts  -->

<div class="footer">

    <div class="box-container">

        <div class="box">
            <h3>branch locations</h3>
            <a href="#">India</a>
            <a href="#">USA</a>
            <a href="#">france</a>

        </div>

        <div class="box">
            <h3>quick links</h3>
            <a href="#">home</a>
            <a href="#about">about</a>
            <a href="#">contact</a>
        </div>

        <div class="box">
            <h3>contact us</h3>
            <p> <i class="fas fa-map-marker-alt"></i> mumbai, india 400104. </p>
            <p> <i class="fas fa-envelope"></i> example@gmail.com </p>
            <p> <i class="fas fa-phone"></i> +123-456-7890 </p>
        </div>

    </div>
</div>

<!-- footer section ends -->



<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

</body>
</html>


