<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Course Com</title>
  <meta name="description" content="The small framework with powerful features">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" type="image/png" href="/favicon.ico" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link href="<?= base_url('assets/css/studentHomePage.min.css'); ?>" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
</head>


<body>

  <header>
    <?= view('homePage/layout/logoutNavbar'); ?>

  </header>

  <section>
  <div class="container-fluid px-0 top-banner">
    <div class="container py-5">
      <div class="row mt-4">

        <?php
        if (!empty($courses)) {
          foreach ($courses as $row) { ?>


            <div class="col-md-3">
              <div class="card">
              <br>
                <div class="card-body">
                <img class="image" src="/uploads/<?php echo $row['course_image']?>" height="200px" width="250px">

                  <h5 class="card-title"><?php echo $row['name'] ?></h5>
                  <p class="card-text"><?php echo $row['description'] ?></p>
                  <a href='<?= base_url('') ?>'><button type="button" class="btn btn-primary ">View Course</button></a>
                </div>
              </div>
            </div>
            
        <?php
          }
        } else {
          echo 'no Courses found';
        }
        ?>
      </div>
    </div>
  </div>
  </section>

</body>

</html>