<form action="<?= base_url('/admin/Courses/Courses/add/') ?>" enctype="multipart/form-data" method="post" id="add" name="add">

  <div class=container>
    <div class="row g-3">
      <div class="col">
        <div class="mb-3">
          <label for="name">Course Name</label>
          <input type="text" class="form-control" id="name" name="name" value='<?= set_value('name'); ?>'>
          <?php if (isset($validation)) : ?>
            <?php if ($validation->hasError('name')) : ?>
              <div class="text-danger" color="red">
                <?= $validation->getError('name'); ?>
              </div>
            <?php endif; ?>
          <?php endif; ?>
        </div>
        <div class="mb-3">
          <label for="name">Description</label>
          <input type="text" class="form-control" id="description" name="description" value='<?= set_value('description'); ?>'>
          <?php if (isset($validation)) : ?>
            <?php if ($validation->hasError('description')) : ?>
              <div class="text-danger" color="red">
                <?= $validation->getError('description'); ?>
              </div>
            <?php endif; ?>
          <?php endif; ?>
        </div>
      </div>
      <div class="col">
        <div class="mb-3">
          <label for="name">Category Name</label>
          <select class="form-select" name="category_id">
            <option value="">Select Category</option>
            <?php foreach ($categories as $category) : ?>
              <option value="<?php echo $category['id']; ?>"> <?php echo $category['name']; ?> </option>"
            <?php endforeach ?>
            <?php if (isset($validation)) : ?>
              <?php if ($validation->hasError('category_name')) : ?>
                <div class="text-danger" color="red">
                  <?= $validation->getError('category_name'); ?>
                </div>
              <?php endif; ?>
            <?php endif; ?>
          </select>
        </div>

        <div class="mb-3">
          <label for="formFileSm" class="form-label">Choose Photo For Course</label>
          <input type="file" name="course_image" id="formFileSm" >
        </div>

      </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Next</button>
</form>
<a href="<?php echo base_url() . '/admin/courses/courses'  ?>"><button type="submit" class="btn btn-back">Back</button></a>