<a href='<?= base_url('admin/Courses/Courses/add/') ?>'><button type="button" class="btn btn-primary ">Add Course</button></a>


<div class="container">
  <div class="row">
    <div class="col-12">
      <table class="table table-bordered">
        <thead>

          <tr>
            <th scope="col">Id</th>
            <th scope="col"> Name</th>
            <th scope="col">Image</th>
            <th scope="col">Description</th>
            <th scope="col">Category name</th>
            <th>Action</th>

          </tr>
        </thead>
        <?php
        if (!empty($courses)) {
          foreach ($courses as $row) { ?>
            <tr>
              <td><?php echo $row['id']; ?></td>
              <td><?php echo $row['name']; ?></td>
              <td>
                <img src="/uploads/<?php echo $row['course_image']?>" height="50px" width="50px">
              </td>
              <td><?php echo $row['description']; ?></td>
              <td><?php echo $row['category_name']; ?></td>


              <td>
                <a href='<?= base_url('/admin/Courses/Courses/edit/' . $row['id']) ?>'> <button type="button" class="btn btn-success"><i class="fas fa-edit"></i></button></a>
                <a href='<?= base_url('/admin/Courses/Courses/delete/' . $row['id']) ?>'> <button type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></button></a>
              </td>
            </tr>
        <?php
          }
        } ?>
        </tbody>
      </table>
      </table>
    </div>
  </div>
</div>
