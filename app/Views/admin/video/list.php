<div class="container">

<a href="<?php echo base_url().'/admin/courses/videos/add/' ?>" class="btn btn-primary">Add</a> 
  <div class="row">
    <div class="col-12">
      <table class="table caption-top">
      <thead>

          <tr>
            <th scope="col">ID</th>
            <th scope="col">Title</th>
            <th scope="col">Course Name</th>
            <th scope="col">Actions</th>

          </tr>
        </thead>
        <?php 
        if(!empty($videos)){
          foreach($videos as $video){?>
          <tr>
            <td><?php echo $video['id']; ?></td>
            <td><?php echo $video['title']; ?></td>
            <td><?php echo $video['course_name']; ?></td>
            <td>
            <a href="<?php echo base_url().'/admin/courses/videos/edit/'.$video['id'] ?>" class="btn btn-sm btn-success"><i class = "fas fa-edit"></i></a>
            <a href="<?php echo base_url().'/admin/courses/videos/delete/'.$video['id'] ?>" class="btn btn-sm btn-danger"><i class = "far fa-trash-alt"></i></a>
            </td>
          </tr>
          <?php
          }
        }?>
    </tbody>
  </table>
      </table>
 
    </div>
  </div>
</div>