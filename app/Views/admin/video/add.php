<form action="<?= base_url('/admin/Courses/Videos/add/') ?>" enctype="multipart/form-data" method="post" id="add" name="add">

  <div class=container>

    <div class="row g-3">

      <div class="col">
        <div class="mb-3">
          <label for="name">Titel</label>
          <input type="text" class="form-control" id="title" name="title" value='<?= set_value('title'); ?>'>
          <?php if (isset($validation)) : ?>
            <?php if ($validation->hasError('title')) : ?>
              <div class="text-danger" color="red">
                <?= $validation->getError('title'); ?>
              </div>
            <?php endif; ?>
          <?php endif; ?>
        </div>
      </div>

      <div class="col">
        <div class="mb-3">
          <label for="name">Course Name</label>
          <select class="form-select" name="course_id">
            <option value="">Select Coure</option>
            <?php foreach ($courses as $course) : ?>
              <option value="<?php echo $course['id']; ?>" <?php echo set_select('course_name', $course['name'], False); ?>> <?php echo $course['name']; ?> </option>
            <?php endforeach ?>
            <?php if (isset($validation)) : ?>
              <?php if ($validation->hasError('course_id')) : ?>
                <div class="text-danger" color="red">
                  <?= $validation->getError('course_id'); ?>
                </div>
              <?php endif; ?>
            <?php endif; ?>
          </select>
        </div>
      </div>
    </div>
  </div>

  <div class=container>

    <div class="row g-3">
      <div class="col-6">
        <div class="mb-3">
          <label for="name">Video Path</label>
          <input type="text" class="form-control" id="path" name="path" value='<?= set_value('path'); ?>'>
          <?php if (isset($validation)) : ?>
            <?php if ($validation->hasError('path')) : ?>
              <div class="text-danger" color="red">
                <?= $validation->getError('path'); ?>
              </div>
            <?php endif; ?>
          <?php endif; ?>
        </div>
      </div>
     
      <div class="col-6">
        <div class="mb-3">
          <br>
          <label for="formFileSm" class="form-label">OR Upload Video</label>
          <input type="file" name="video" id="formFileSm">
          <?php if (isset($validation)) : ?>
            <?php if ($validation->hasError('upload_video')) : ?>
              <div class="text-danger" color="red">
                <?= $validation->getError('upload_video'); ?>
              </div>
            <?php endif; ?>
          <?php endif; ?>
        </div>
      </div>

    </div>
  </div>


  <button type="submit" class="btn btn-save">Add</button>
</form>
<a href="<?php echo base_url() . '/admin/courses/videos'  ?>"><button type="submit" class="btn btn-back">Back</button></a>

