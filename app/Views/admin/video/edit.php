<form action="<?= base_url('/admin/Courses/Videos/edit/' . $video['id']) ?>" enctype="multipart/form-data" method="post" id="edit" name="edit">

    <div class=container>
        <div class="row g-3">
            <div class="col">
                <div class="mb-3">
                    <label for="name">Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="<?php echo set_value('title',$video['title']) ?>">
                    <?php if (isset($validation)) : ?>
                        <?php if ($validation->hasError('title')) : ?>
                            <div class="text-danger" color="red">
                                <?= $validation->getError('title'); ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>

            </div>
            <div class="col">
                <div class="mb-3">
                    <label for="name">Course Name</label>
                    <select class="form-select" name="course_id">
                        <option value="">Select Coure</option>
                        <?php foreach ($courses as $course) : ?>
                            <option value="<?php echo $course['id']; ?>" <?php echo set_select('course_name', $course['name'], False); ?>> <?php echo $course['name']; ?> </option>
                        <?php endforeach ?>
                        <?php if (isset($validation)) : ?>
                            <?php if ($validation->hasError('course_name')) : ?>
                                <div class="text-danger" color="red">
                                    <?= $validation->getError('course_name'); ?>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    
    <button type="submit" class="btn btn-save">Update</button>
</form>
<a href="<?php echo base_url() . '/admin/courses/videos'  ?>"><button type="submit" class="btn btn-back">Back</button></a>