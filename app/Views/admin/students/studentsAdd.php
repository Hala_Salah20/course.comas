<div class="container">
   <?php

$validation = \Config\Services::validation();

?>


           
           
           <form action="<?php echo base_url("/Admin/Students/add")?>" method="post">

           <div class="row">
          <div class="col">
         <label for="email" class="form-label">Email</label>
         <input type="email" name ="email" class="form-control" id="email" aria-describedby="emailHelp" value="<?php echo set_value('email'); ?>">

    <?php if(isset($validation)):?>
      <?php if($validation->hasError('email')): ?>
        <div class="text-danger" color="red">
        <?= $validation->getError('email'); ?>
        </div>
       <?php endif;?>
       <?php endif;?>   
  </div>
  <div class="col">
  <label for="password" class="form-label">Password</label>
    <input type="password" name="password" class="form-control" id="password" value="<?php echo set_value('password'); ?>">

    <?php if(isset($validation)):?>
      <?php if($validation->hasError('password')): ?>
        <div class="text-danger" color="red">
        <?= $validation->getError('password'); ?>
        </div>
       <?php endif;?>
       <?php endif;?>
           </div>
           </div>
           <div class="row">
          <div class="col">
          <label for="user_name" class="form-label">User Name</label>
    <input type="user_name" name="user_name" class="form-control" id="user_name" value="<?php echo set_value('user_name'); ?>">

    <?php if(isset($validation)):?>
      <?php if($validation->hasError('user_name')): ?>
        <div class="text-danger" color="red">
        <?= $validation->getError('user_name'); ?>
        </div>
       <?php endif;?>
       <?php endif;?>

  </div>
  <div class="col">
    <label for="pass_confirm" class="form-label">Confirm Password</label>
    <input type="password" name="pass_confirm" class="form-control" id="pass_confirm" value="<?php echo set_value('pass_confirm'); ?>" >
    <?php if(isset($validation)):?>
      <?php if($validation->hasError('pass_confirm')): ?>
        <div class="text-danger" color="red">
        <?= $validation->getError('pass_confirm'); ?>
        </div>
       <?php endif;?>
       <?php endif;?>
      </div>
           </div>
  <button type="submit" class="btn btn-save">Add</button>
</form>
<a href="<?php echo base_url().'/admin/students'  ?>"><button type="submit" class="btn btn-back">Back</button></a>

      </div