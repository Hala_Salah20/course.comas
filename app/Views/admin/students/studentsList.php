<div class="container">
<?php

$session = \Config\Services::session();

if($session->getFlashdata('success'))
{
    echo '
    <div class="alert alert-success">'.$session->getFlashdata("success").'</div>
    ';
}
?>
<a href="<?php echo base_url().'/admin/students/add' ?>" class="btn btn-primary">Add</a> 

  <div class="row">
    <div class="col-12">
    <table class="table caption-top">
    <thead class="thead-dark">

          <tr>
            <th scope="col">ID</th>
            <th scope="col">Email</th>
            <th scope="col">User Name</th>
            <th scope="col">Is Active</th>
            <th scope="col">Ban</th>
            <th scope="col">Actions</th>

          </tr>
        </thead>
        <?php 
        if(!empty($students)){
          foreach($students as $student){?>
          <tr>
            <td><?php echo $student['id']; ?></td>
            <td><?php echo $student['email']; ?></td>
            <td><?php echo $student['user_name']; ?></td>
            <td><?php if($student['is_active'] == 1 ){ echo 'Yes'; } else echo 'No'; ?></td> 
            <td><?php if($student['ban'] == 1 ){ echo 'Yes'; } else echo 'No'; ?></td>         
        

            <td>
            <a href="<?php echo base_url().'/admin/students/edit/'.$student["id"] ?>" class="btn btn-sm btn-success"><i class = "fas fa-edit"></i></a>
            <a href="<?php echo base_url().'/admin/students/delete/'.$student["id"] ?>" class="btn btn-sm btn-danger"><i class = "far fa-trash-alt"></i></a>
            </td>
          </tr>
          <?php
          }
        }?>
    </tbody>
  </table>
      </table>
 
    </div>
  </div>
</div>

