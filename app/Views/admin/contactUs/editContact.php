<div class="container">
   <?php

$validation = \Config\Services::validation();

?>           
  <form action= "<?= base_url('/admin/ContactUs/edit/'.$dataa['id'])?>"method="post">

  <div class="col">
  <label for="title" class="form-label">Title</label>
    <input type="title" name="title" class="form-control" id="title" value="<?php echo set_value('title',$dataa['title']); ?>" >

    <?php if(isset($validation)):?>
      <?php if($validation->hasError('title')): ?>
        <div class="text-danger" color="red">
        <?= $validation->getError('title'); ?>
        </div>
       <?php endif;?>
       <?php endif;?>
           </div>
           
   
          <div class="col">
          <label for="link" class="form-label"> Link</label>
    <input type="link" name="link" class="form-control" id="link" value="<?php echo set_value('link',$dataa['link']); ?>">

    <?php if(isset($validation)):?>
      <?php if($validation->hasError('link')): ?>
        <div class="text-danger" color="red">
        <?= $validation->getError('link'); ?>
        </div>
       <?php endif;?>
       <?php endif;?>

  </div>  
  <div class="col">
  <label class="form-label">Icone Code</label>
          <input type="code" id="icon_code" name="icon_code"value="<?php echo set_value('icon_code',$dataa['icon_code']);?>"   />
          <?php if(isset($validation)):?>
      <?php if($validation->hasError('icon_code')): ?>
        <div class="text-danger" color="red">
        <?= $validation->getError('icon_code'); ?>
        </div>
       <?php endif;?>
       <?php endif;?>   
  </div>
  <button type="submit" class="btn btn-save">save</button>
</form>
<a href="<?php echo base_url().'/admin/contactus'  ?>"><button type="submit" class="btn btn-back">Back</button></a>

      </div