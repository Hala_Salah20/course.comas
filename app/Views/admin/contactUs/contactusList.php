

<a href='<?= site_url('/admin/ContactUs/add') ?>'>
  <button type="button" class="btn btn-primary">Add</button></a>

<br>
<br>
<div class="container">
  <div class="row">
    <div class="col-12">
    <table class="table caption-top">
        <thead>
          <tr>
          <th scope="col">id</th>
            <th scope="col">Icon Code</th>
            <th scope="col">Title</th>
            <th scope="col">Link</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
       
          <?php
          if (!empty($contacts)) {
            foreach ($contacts as $contact) { ?>
              <tr>
                 <td><?php echo $contact['id']; ?></td>
                <td><i class="<?php echo set_value('icon_code',$contact['icon_code']) ?>"></i></td>
                <td><?php echo $contact['title']; ?></td>
                <td><?php echo $contact['link']; ?></td>
                <td>
                  <a href='<?= base_url('/admin/ContactUs/edit/' . $contact['id']) ?>'>
                    <button type="button" title="Edit" class="btn btn-success"><i class="fas fa-edit"></></i></button></a>

                  <a href='<?= base_url('/admin/ContactUs/delete/' . $contact['id']) ?>'>
                    <button type="button" title="delete" class="btn btn-danger"><i class="far fa-trash-alt"></i></button></a>


                </td>
              </tr>
          <?php
            }
          } ?>

        </tbody>
      </table>
    </div>
  </div>
</div>