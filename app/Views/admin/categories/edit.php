<form action="<?php echo base_url("/Admin/Courses/Categories/edit/".$category['id'])?>" method="post">
  <?= csrf_field() ?>

  <div class="mb-3">
    <label for="InputName" class="form-label">Name</label>
    <input type="name" class="form-control" name="name" value="<?php echo set_value('name', $category['name']); ?>" id="name">

    <?php if(isset($validation)):?>
      <?php if($validation->hasError('name')): ?>
        <div class="text-danger" color="red">
          <?= $validation->getError('name'); ?>
        </div>
       <?php endif;?>
       <?php endif;?>
  </div>

  <div class="mb-3">
    <label for="InputDescription" class="form-label">Description</label>
    <input type="description" class="form-control" name="description" value="<?php echo set_value('description', $category['description'] ); ?>" id="description">
 
    <?php if(isset($validation)):?>
      <?php if($validation->hasError('description')): ?>
        <div class="text-danger" color="red">
          <?= $validation->getError('description'); ?>
        </div>
       <?php endif;?>
       <?php endif;?>
  </div>

  <button type="submit" class="btn btn-primary">Save</button>
</form>
<a href="<?php echo base_url().'/admin/Courses/categories'  ?>"><button type="submit" class="btn btn-back">Back</button></a>
</a>