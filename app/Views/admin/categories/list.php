<a href='<?= site_url('/admin/Courses/Categories/add') ?>'>
  <button type="button" class="btn btn-primary">Add Category</button></a>

<br>
<br>
<div class="container">
  <div class="row">
    <div class="col-12">
    <table class="table caption-top">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Language code</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
        
          <?php
          if (!empty($categories)) {
            foreach ($categories as $categories) { ?>
              <tr>
                <td><?php echo $categories['id']; ?></td>
                <td><?php echo $categories['name']; ?></td>
                <td><?php echo $categories['description']; ?></td>
                <td><?php echo $categories['language_code']; ?></td>
                <td>
                  <a href='<?= base_url('/admin/Courses/Categories/edit/' . $categories['id']) ?>'>
                    <button type="button" title="Edit" class="btn btn-success"><i class="fas fa-edit"></></i></button></a>

                  <a href='<?= base_url('/admin/Courses/Categories/delete/' . $categories['id']) ?>'>
                    <button type="button" title="delete" class="btn btn-danger"><i class="far fa-trash-alt"></i></button></a>


                </td>
              </tr>
          <?php
            }
          } ?>

        </tbody>
      </table>
    </div>
  </div>
</div>