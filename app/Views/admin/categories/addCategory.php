<form action="<?php echo base_url("/Admin/Courses/Categories/add") ?>" enctype="multipart/form-data" method="post" id="add" name="add">
  <?= csrf_field() ?>
  <div class=container>
    <div class="row g-3">
      <div class="col">
        <div class="mb-3">
          <label for="InputName" class="form-label">Name</label>
          <input type="name" class="form-control" name="name" value="<?php echo set_value('name'); ?>" id="name">

          <?php if (isset($validation)) : ?>
            <?php if ($validation->hasError('name')) : ?>
              <div class="text-danger" color="red">
                <?= $validation->getError('name'); ?>
              </div>
            <?php endif; ?>
          <?php endif; ?>


        </div>


        <div class="mb-3">
          <label for="InputDescription" class="form-label">Description</label>
          <input type="description" class="form-control" name="description" value="<?php echo set_value('description'); ?>" id="description">

          <?php if (isset($validation)) : ?>
            <?php if ($validation->hasError('description')) : ?>
              <div class="text-danger" color="red">
                <?= $validation->getError('description'); ?>
              </div>
            <?php endif; ?>
          <?php endif; ?>
        </div>
        <div class="col">
          <div class="mb-3">
            <label for="formFileSm" class="form-label">Choose Photo For Category</label>
            <input type="file" name="category_image" id="formFileSm">
            <?php if (isset($validation)) : ?>
              <?php if ($validation->hasError('category_image')) : ?>
                <div class="text-danger" color="red">
                  <?= $validation->getError('category_image'); ?>
                </div>
              <?php endif; ?>
            <?php endif; ?>
          </div>


        </div>
      </div>
      <button type="submit" class="btn btn-save">Add</button>

</form>
<a href="<?php echo base_url() . '/admin/Courses/categories'  ?>"><button type="submit" class="btn btn-back">Back</button></a>
</a>