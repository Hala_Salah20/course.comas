<div class="container">

<a href="<?php echo base_url().'/admin/admins/add' ?>" class="btn btn-primary">Add</a> 
  <div class="row">
    <div class="col-12">
      <table class="table caption-top">
      <thead>

          <tr>
            <th scope="col">ID</th>
            <th scope="col">Email</th>
            <th scope="col">User Name</th>
            <th scope="col">Is Active</th>
            <th scope="col">Actions</th>

          </tr>
        </thead>
        <?php 
        if(!empty($admins)){
          foreach($admins as $admins){?>
          <tr>
            <td><?php echo $admins['id']; ?></td>
            <td><?php echo $admins['email']; ?></td>
            <td><?php echo $admins['user_name']; ?></td>
            <td><?php if($admins['is_active'] == 1 ){ echo 'Yes'; } else echo 'No'; ?></td>
            <td>
            <a href="<?php echo base_url().'/admin/admins/edit/'.$admins["id"] ?>" class="btn btn-sm btn-success"><i class = "fas fa-edit"></i></a>
            <a href="<?php echo base_url().'/admin/admins/delete/'.$admins["id"] ?>" class="btn btn-sm btn-danger"><i class = "far fa-trash-alt"></i></a>
            </td>
          </tr>
          <?php
          }
        }?>
    </tbody>
  </table>
      </table>
 
    </div>
  </div>
</div>