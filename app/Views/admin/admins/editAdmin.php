<form action="<?php echo base_url("/Admin/Admins/edit/".$admin['id'])?>" method="post">
<div class = container>
  <div class="row g-3">
    <div class="col">
      <div class="mb-3">
        <label for="email" class="form-label">Email address</label>
        <input type="email" name ="email" class="form-control" id="email" aria-describedby="emailHelp" value="<?php echo set_value('email' , $admin['email']); ?>">
        <?php if(isset($validation)):?>
          <?php if($validation->hasError('email')): ?>
            <div class="text-danger" color="red">
              <?= $validation->getError('email'); ?>
            </div>
            <?php endif;?>
            <?php endif;?>
          </div>
          <div class="mb-3">
            <label for="email" class="form-label">User Name</label>
            <input type="text" name ="user_name" class="form-control" id="user_name"  value="<?php echo set_value('user_name' , $admin['user_name']); ?>">
            <?php if(isset($validation)):?>
              <?php if($validation->hasError('user_name')): ?>
                <div class="text-danger" color="red">
                  <?= $validation->getError('user_name'); ?>
                </div>
                <?php endif;?>
                <?php endif;?>   
          </div>
    </div>
    
    <div class="col">
      <div class="mb-3">
        <label for="password" class="form-label">Password</label>
        
        <input type="password" name="password" class="form-control" id="password" >
        <?php if(isset($validation)):?>
          <?php if($validation->hasError('password')): ?>
            <div class="text-danger" color="red">
              <?= $validation->getError('password'); ?>
            </div>
            <?php endif;?>
            <?php endif;?>
          </div>
          
          <div class="mb-3">
            <label for="pass_confirm" class="form-label">Confirm Password</label>
            <input type="password" name="pass_confirm" class="form-control" id="pass_confirm" >
            <?php if(isset($validation)):?>
              <?php if($validation->hasError('pass_confirm')): ?>
                <div class="text-danger" color="red">
                  <?= $validation->getError('pass_confirm'); ?>
                </div>
                <?php endif;?>
                <?php endif;?>
              </div>  
              <div class="form-check form-switch"> 
                                                                             
                <input class="form-check-input" name="is_active" type="checkbox" id="is_active"<?php  if($admin['is_active'] == 1 ){ echo set_checkbox('is_active', '1', true); } else set_checkbox('is_active', '0', false);?>>
                <label class="form-check-label" for="flexSwitchCheckChecked">Active</label>
              </div>
            </div>
          </div>
        </div>
  <button type="submit" class="btn btn-save">Add</button>
</form>
<a href="<?php echo base_url().'/admin/admins'  ?>"><button type="submit" class="btn btn-back">Back</button></a>
</a>
