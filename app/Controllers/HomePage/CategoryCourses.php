<?php
namespace App\Controllers\HomePage;

use App\Controllers\HomePage\BaseController;
use App\Models\CoursesModel;

class CategoryCourses extends BaseController
{
    public function index()
    { 
       return view('homepage/categoryCourses');
    }

    public function getCourses($id)
    {

        $model = new CoursesModel();
        $courses = $model->categoryCourses($id);
        $data["courses"] = $courses;

       return view('homepage/categoryCourses',$data);
    }
}