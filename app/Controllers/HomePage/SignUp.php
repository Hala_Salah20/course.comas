<?php

namespace App\Controllers\HomePage;

use App\Controllers\BaseController;

use App\Models\StudentsModel;

class SignUp extends BaseController
{
	public function index()
	{

		return view('homepage/signup');
	}

	function  registration()
	{

		if ($this->request->getMethod() == 'post') {

			$rules = $this->validate([

				'email'	=>	'required|valid_email|is_unique[students.email]',
				'user_name'	=> 'required|min_length[3]|max_length[32]|is_unique[students.user_name]|alpha_space',
				'password'	=>	'required|min_length[8]|max_length[35]',
				'pass_confirm' => 'required|matches[password]'
			]);

			if ($rules) {
				$students = new StudentsModel();
				$data = [
					'email'	=>	$this->request->getPost('email'),
					'user_name' =>	$this->request->getPost('user_name'),
					'password'	=> password_hash($this->request->getPost('password'), PASSWORD_DEFAULT)
				];

				$students->AddStudent($data);
				$session = \Config\Services::session();
				$session->setFlashdata('success', 'Done registration');
				return $this->response->redirect(base_url("/homepage/login"));
			} else {
				$data['error'] = $this->validator;
				return view(('homepage/signup'), $data);
			}
		}

		return view(('homepage/signup'));
	}
}
