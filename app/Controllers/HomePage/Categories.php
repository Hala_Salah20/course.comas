<?php


namespace App\Controllers\HomePage;

use App\Controllers\HomePage\BaseController;
use App\Models\CategoriesModel;
use CodeIgniter\Validation\Rules;

class Categories extends BaseController
{
    public function index()
    { 
        $model = new CategoriesModel();
        $categories = $model->getCategories();
        $data["categories"] = $categories;

       return view('homepage/categoriesPage',$data);

    }
}