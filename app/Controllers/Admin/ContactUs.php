<?php

namespace App\Controllers\Admin;

use App\Models\ContactUsModel;

class  ContactUS extends BaseController
{
    public function index()
    {
        $contactModel = new ContactUsModel();
        $data["contacts"] =  $contactModel->getContacts();;
        $this->loadTemplate('contactus/contactusList', lang('contactUs/list.page_title'), $data);
    }

    public function add()
    {
        $contactModel = new ContactUsModel();
        if ($this->request->getMethod() == 'post') {
            $rules = $this->validate([
                'icon_code'    => 'required|min_length[3]|max_length[250]',
                'title'    => 'required|min_length[3]|max_length[35]|alpha_space',
                'link'    =>    'required|min_length[8]|max_length[255]|valid_url',
            ]);
            if ($rules) {
                $data = [
                    'title' =>    $this->request->getPost('title'),
                    'link'    => $this->request->getPost('link'),
                    'icon_code' => $this->request->getPost('icon_code'),
                ];

                $contactModel->addContacts($data);
                $session = \Config\Services::session();
                $session->setFlashdata('success', 'Added');
                return $this->response->redirect(site_url('/admin/contactus'));
            } else {
                $data['error'] = $this->validator;
                $this->loadTemplate('contactus/addContact', lang('contactUs/new.page_title'), $data);
            }
        }
        $this->loadTemplate('contactus/addContact', lang(' contactUs/new.page_title '));
    }

    public function edit($id)
    {
        $contactModel = new  ContactUsModel();
        if ($this->request->getMethod() == 'post') {
            $rules = $this->validate([
                'icon_code'    => 'required|min_length[3]|max_length[255]',
                'title'    => 'required|min_length[3]|max_length[32]|alpha_space',
                'link'    =>    'required|valid_url|min_length[8]|max_length[255]',

            ]);
            if (!$rules) {
                $data['dataa'] =  $contactModel->getContact($id);
                $data['error'] = $this->validator;
                $this->loadTemplate('contactUs/editContact', lang('contactUs/edit.page_title '), $data);
            } else {

                $data = [
                    'title' =>    $this->request->getPost('title'),
                    'link'    => $this->request->getPost('link'),
                    'icon_code' => $this->request->getPost('icon_code'),
                ];
            }
            $contactModel->editContacts($id, $data);
            $session = \Config\Services::session();
            $session->setFlashdata('success', ' Data Updated');

            return $this->response->redirect(site_url('/admin/contactus'));
        } else {
            $data['dataa'] = $contactModel->getContact($id);
            $this->loadTemplate('contactUs/editContact', lang('contactUs/list.page_title '), $data);
        }
    }

    public function delete($id)
    {
        $contactModel = new ContactUsModel();
        $contactModel->deleteContacts($id);
        $session = \Config\Services::session();
        $session->setFlashdata('success', ' Data Deleted');
        return $this->response->redirect(site_url('/admin/contactus'));
    }
}
