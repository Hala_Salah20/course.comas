<?php 
 namespace App\Controllers\Admin;
 use App\Models\StudentsModel;
class Students extends BaseController
{

public function index()
{
    $model = new StudentsModel ();
    $student = $model->getStudent();
    $data["students"] = $student;
    $this->loadTemplate('students/studentsList',lang('Students List'),$data);
}

function add()
{
    if ($this->request->getMethod()=='post'){
        $rules = $this->validate([
            'email'	=>	'required|valid_email|is_unique[students.email]',
            'user_name'	=>'required|min_length[3]|max_length[32]|is_unique[students.user_name]|alpha_space',
			'password'	=>	'required|min_length[8]|max_length[16]',
            'pass_confirm' => 'required|matches[password]']);
            if ($rules) {
                $students = new StudentsModel ();
                $data=['email'	=>	$this->request->getPost('email'),
                       'user_name'=>	$this->request->getPost('user_name'),
				       'password'	=> password_hash($this->request->getPost('password'),PASSWORD_DEFAULT)
                    ];
                       $students ->AddStudent($data);
                       $session = \Config\Services::session();
                       $session->setFlashdata('success', 'Student Data Added');
                       return $this->response->redirect(site_url('/admin/students/index'));
                    } else {
                        $data['error'] = $this->validator;
                        $this->loadTemplate('students/studentsAdd',lang('StudentsList'),$data);
                    }
                } $this->loadTemplate('students/studentsAdd',lang(' Add Student '));
}

    
 
    function edit($id)
    {
    
        if ($this->request->getMethod()=='post'){
        $rules = $this->validate([
            'email' => 'required|max_length[255]|is_unique[students.email , id ,'.$id.']',
            'user_name' => 'required|min_length[8]|max_length[35]|is_unique[students.user_name , id , ' .$id. ']|alpha_space',
			'password'	=>	'max_length[16]',
            'pass_confirm' => 'matches[password]',
          
        ]);

        $students = new StudentsModel();

        if(!$rules)
        {
        	$data['studentData'] = $students->singleData($id);
        	$data['error'] = $this->validator;
            $this->loadTemplate('students/studentsEdit',lang('Edit Student'),$data);
        } else 
        {
            $data=[];
           
            $password =$this->request->getPost('password');
            if ($password==''){
                $data=[
                    'email'	=>	$this->request->getPost('email'),
                    'user_name'=>	$this->request->getPost('user_name'),
                    'is_active'=>(isset($_POST['is_active'])) ? 1 : 0];
            }
            else{
                $data=[
                    'email'	=>	$this->request->getPost('email'),
                    'user_name'=>	$this->request->getPost('user_name'),
                    'password'	=> password_hash($this->request->getPost('password'),PASSWORD_DEFAULT),
                    'is_active'=>(isset($_POST['is_active'])) ? 1 : 0
                  
                ];
      
            }
        
        	$students->updateStudent($id, $data);
        	$session = \Config\Services::session();
            $session->setFlashdata('success', 'Student Data Updated');

        	return $this->response->redirect(site_url('/admin/students/index'));
        }
    }
    else{
            $student = new StudentsModel();
            $data['studentData'] = $student->singleData($id);
            $this->loadTemplate('students/studentsEdit',lang('Edit Student')
            
            ,$data);

    }
}
    function delete($id)
    {
        $students = new StudentsModel();

        $students-> deleteStudent($id);

        $session = \Config\Services::session();

        $session->setFlashdata('success', 'Student Data Deleted');

        return $this->response->redirect(site_url('/admin/students/index'));
    }
}

?>