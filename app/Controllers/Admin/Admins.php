<?php

namespace App\Controllers\Admin;

use App\Models\AdminsModel;

class Admins extends BaseController
{
    /**
     * This function well offer the default page => list of admins
     *
     * @return void
     */
    public function index()
    {
        // I want to offer admins list page
        //go to model file to access the data base
        $adminModel = new AdminsModel();
        // but the data base in array
        $data["admins"]  = $adminModel->getAdmins();
        //send data to view folder "admins is key to use in view file"
        $this->loadTemplate('admins/adminsList', lang('admin/list.page_title'), $data);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function add()
    {
        $adminModel = new AdminsModel();
        //to add row to data base u should go to model to access database
        if ($this->request->getMethod() === 'post') {
            //set rules to input value
            $rules = [
                'email' => 'required|max_length[255]|is_unique[admins.email]',
                'user_name' => 'required|min_length[8]|max_length[35]|is_unique[admins.user_name]|alpha_space',
                'password'  => 'required|min_length[8]|max_length[35]',
                'pass_confirm' => 'required|matches[password]',
            ];
            //check if rules beccome true ot not
            if ($this->validate($rules)) {
                $adminModel->saveAdmin([
                    'email' => $this->request->getPost('email'),
                    'user_name' => $this->request->getPost('user_name'),
                    'password'    => password_hash($this->request->getPost('password'), PASSWORD_DEFAULT)
                ]);
                return redirect()->to('/admin/admins');
            }/*if validate */ else {
                $data['validation'] = $this->validator;
                $this->loadTemplate('admins/addAdmin', lang('admin/new.page_title'), $data);
            } //else
        }
        /**if post */
        else {
            $this->loadTemplate('admins/addAdmin', lang('admin/new.page_title'));
        } //else

    } //add fuction
    public function edit($id)
    {
        $adminModel = new AdminsModel();
        //to add row to data base u should go to model to access database
        if ($this->request->getMethod() === 'post') {
            $rules = [
                'email' => 'required|max_length[255]|is_unique[admins.email , id ,' . $id . ']',
                'user_name' => 'required|min_length[8]|max_length[35]|is_unique[admins.user_name , id , ' . $id . ']|alpha_space',
                'password'  => 'max_length[35]',
                'pass_confirm' => 'matches[password]'
            ];

            if ($this->validate($rules)) {
                $data = [];
                $password = $this->request->getPost('password');
                if ($password == '') {
                    $data = [
                        'email'    =>    $this->request->getPost('email'),
                        'user_name' =>    $this->request->getPost('user_name'),
                        'is_active' => (isset($_POST['is_active'])) ? 1 : 0
                    ];
                } else {
                    $data = [
                        'email'    =>    $this->request->getPost('email'),
                        'user_name' =>    $this->request->getPost('user_name'),
                        'password'    => password_hash($this->request->getPost('password'), PASSWORD_DEFAULT),
                        'is_active' => (isset($_POST['is_active'])) ? 1 : 0
                    ];
                }
                $adminModel->editAdmin($id, $data);
                return redirect()->to(base_url("/admin/admins"));
            }/*if validate */ else {
                $data['admin'] = $adminModel->getAdmin($id);
                $data['validation'] = $this->validator;
                $this->loadTemplate('admins/editAdmin', lang('admin/edit.page_title'), $data);
            }
        }
        /**if post */
        else {
            $admin['admin'] = $adminModel->getAdmin($id);
            $this->loadTemplate('admins/editAdmin', lang('admin/edit.page_title'), $admin);
        } //else
    } //edit function


    public function delete($id)
    {
        $adminModel = new AdminsModel();
        $adminModel->deleteAdmin($id);
        return redirect()->to('/admin/admins');
    } //delete function

}//class
