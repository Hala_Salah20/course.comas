<?php

namespace App\Controllers\Admin\Courses;

use App\Controllers\Admin\BaseController;
use App\Models\VideosModel;
use App\Models\CoursesModel;

class Videos extends BaseController
{
    public function index()
    {
        // I want to offer videos list page
        //go to model file to access the data base
<<<<<<< HEAD
        $videoModel = new VideosModel();
        // but the data base in array
        $videosData["videos"]  = $videoModel->getVideos();
=======
<<<<<<< HEAD
        $videoModel = new VideosModel();
        // but the data base in array
        $videosData["videos"]  = $videoModel->getVideos();
=======
<<<<<<< HEAD
        $videoModel = new VideosModel();
        // but the data base in array
        $videosData["videos"]  = $videoModel->getVideos();
=======
<<<<<<< HEAD
        $videoModel = new VideosModel();
        // but the data base in array
        $videosData["videos"]  = $videoModel->getVideos();
=======
<<<<<<< HEAD
        $videoModel = new VideosModel();
        // but the data base in array
        $videosData["videos"]  = $videoModel->getVideos();
=======
        $model = new VideosModel();
        // but the data base in array
        $videosData["videos"]  = $model->getVideos();
>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
        //send data to view folder "videos is key to use in view file"
        $this->loadTemplate('video/list', lang('video/list.page_title'), $videosData);
    }


    public function add()
    {
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
        $videoModel = new VideosModel();
        if ($this->request->getMethod() === 'post') {
            $rules = $this->validate([
                'title' => 'required|min_length[8]|max_length[35]|is_unique[videos.title]',
                'course_id' => 'required',
                'video' => [
                    'rules' => 'uploaded[video]|mime_in[video,video/mp4]',
                    'errors' => [
                        'uploaded' => 'Video Not Uploaded.',
                        'mime_in' => 'The file uploaded is not mp4 Extention.'
                    ]
                ],
                'path' => [
                    'rules' => 'required|valid_url',
                    'errors' => [
                        'valid_url' => 'This URL is not Valid.',
                    ]
                ]
            ]);
            if ($rules) {
                $file = $this->request->getFile('video');
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
=======
        $videos = new VideosModel();
        if ($this->request->getMethod() === 'post') {
            $rules = [
                'title' => 'required|min_length[8]|max_length[35]|is_unique[videos.title]',
                'course_id' => 'required',
                'video' => 'uploaded[video]',
                'path' => 'required|valid_url',

            ];

            if ($this->validate($rules)) {

                $file = $this->request->getFile('video');

>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
                if ($file->isValid() && !$file->hasMoved()) {
                    $videoName = $file->getRandomName();
                    $file->move('uploads/videos/', $videoName);
                }
                $data = [
                    'title' => $this->request->getPost('title'),
                    'course_id'  => $this->request->getPost('course_id'),
                    'upload_video' => $videoName
                ];
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
                $videoModel->insertvideo($data);
                return redirect()->to(base_url("/admin/Courses/Videos"));
            } else {
                $data['courses'] = $videoModel->getCourses();
<<<<<<< HEAD
                $data['validation'] = $this->validator;
                $this->loadTemplate('video/add', lang('video/new.page_title'), $data);
            }
        } else {
            $data['courses'] = $videoModel->getCourses();
            $this->loadTemplate('video/add', lang('video/new.page_title'), $data);
        }
    }

    public function edit($id)
    {
        $videoModel = new VideosModel();
        if ($this->request->getMethod() === 'post') {
            $rules = [
                'title' => 'required|min_length[8]|max_length[35]|is_unique[videos.title]',
                'course_id' => 'required',
            ];
            if ($this->validate($rules)) {
                $data =  [
                    'title' => $this->request->getPost('title'),
                    'course_id'  => $this->request->getPost('course_id'),
                ];
                $videoModel->editVideo($id, $data);
                return redirect()->to(base_url("/admin/Courses/Videos"));
            } else {
                $data['validation'] = $this->validator;
                $data['video'] = $videoModel->getVideo($id);
                $data['courses'] = $videoModel->getCourses();
                $this->loadTemplate('video/edit', lang('Edit Video'), $data);
            }
        }
        $data['video'] = $videoModel->getVideo($id);
        $data['courses'] = $videoModel->getCourses();
        $this->loadTemplate('video/edit', lang('video/edit.page_title'), $data);
    }

    public function delete($id)
    {
        $videoModel = new VideosModel();
        $videoModel->deleteVideo($id);
        return redirect()->to(base_url("/admin/Courses/Videos"));
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
=======
                $videos->insertvideo($data);
                return redirect()->to(base_url("/admin/Courses/Videos"));
            } else {
                $data['courses'] = $videos->getCourses();
>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
                $data['validation'] = $this->validator;
                $this->loadTemplate('video/add', lang('video/new.page_title'), $data);
            }
        } else {
<<<<<<< HEAD
            $data['courses'] = $videoModel->getCourses();
=======
<<<<<<< HEAD
            $data['courses'] = $videoModel->getCourses();
=======
<<<<<<< HEAD
            $data['courses'] = $videoModel->getCourses();
=======
<<<<<<< HEAD
            $data['courses'] = $videoModel->getCourses();
=======
            $data['courses'] = $videos->getCourses();
>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
            $this->loadTemplate('video/add', lang('video/new.page_title'), $data);
        }
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
    }

    public function edit($id)
    {
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
        $videoModel = new VideosModel();
        if ($this->request->getMethod() === 'post') {
            $rules = [
                'title' => 'required|min_length[8]|max_length[35]|is_unique[videos.title]',
                'course_id' => 'required',
            ];
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
=======
        $videos = new VideosModel();
        if ($this->request->getMethod() === 'post') {
            $rules = [

                'title' => 'required|min_length[8]|max_length[35]|is_unique[videos.title]',
                'course_id' => 'required',

            ];

>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
            if ($this->validate($rules)) {
                $data =  [
                    'title' => $this->request->getPost('title'),
                    'course_id'  => $this->request->getPost('course_id'),
                ];
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
                $videoModel->editVideo($id, $data);
                return redirect()->to(base_url("/admin/Courses/Videos"));
            } else {
                $data['validation'] = $this->validator;
                $data['video'] = $videoModel->getVideo($id);
                $data['courses'] = $videoModel->getCourses();
                $this->loadTemplate('video/edit', lang('Edit Video'), $data);
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
            }
        }
        $data['video'] = $videoModel->getVideo($id);
        $data['courses'] = $videoModel->getCourses();
        $this->loadTemplate('video/edit', lang('video/edit.page_title'), $data);
<<<<<<< HEAD
    }

    public function delete($id)
    {
        $videoModel = new VideosModel();
        $videoModel->deleteVideo($id);
        return redirect()->to(base_url("/admin/Courses/Videos"));
=======
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
    }

    public function delete($id)
    {
        $videoModel = new VideosModel();
        $videoModel->deleteVideo($id);
        return redirect()->to(base_url("/admin/Courses/Videos"));
=======
            }
        }
        $data['video'] = $videoModel->getVideo($id);
        $data['courses'] = $videoModel->getCourses();
        $this->loadTemplate('video/edit', lang('video/edit.page_title'), $data);
<<<<<<< HEAD
    }

    public function delete($id)
    {
        $videoModel = new VideosModel();
        $videoModel->deleteVideo($id);
        return redirect()->to(base_url("/admin/Courses/Videos"));
=======
=======
                $videos->editVideo($id, $data);
                return redirect()->to(base_url("/admin/Courses/Videos"));
            } else {
                $data['validation'] = $this->validator;
                $data['video'] = $videos->getVideo($id);
                $data['courses'] = $videos->getCourses();
                $this->loadTemplate('video/edit', lang('Edit Video'), $data);
            }
        }
        $data['video'] = $videos->getVideo($id);
        $data['courses'] = $videos->getCourses();
        $this->loadTemplate('video/edit', lang('Edit Video'), $data);
>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
    }

    public function delete($id)
    {
<<<<<<< HEAD
        $videoModel = new VideosModel();
        $videoModel->deleteVideo($id);
=======
        $Video = new VideosModel();
        $Video->deleteVideo($id);
>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
        return redirect()->to(base_url("/admin/Courses/Videos"));
    }

    

}
