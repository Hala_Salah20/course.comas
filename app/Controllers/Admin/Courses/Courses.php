<?php

namespace App\Controllers\Admin\Courses;

use App\Controllers\Admin\BaseController;
use App\Models\CoursesModel;


class Courses extends BaseController
{
    // show all courses in courses list
    public function index()
    {
        $courseModel = new CoursesModel();
        $courses = $courseModel->getCourses();
        $data['courses'] = $courses;
        $this->loadTemplate('courses/coursesList', lang('coruses/list.page_title'), $data);
    }

    // insert data
    public function add()
    {
        $courseModel = new CoursesModel();
        if ($this->request->getMethod() === 'post') {
<<<<<<< HEAD
            $rules = $this->validate([
                'name' => 'required|is_unique[courses.name]',
                'description' => 'required',
                'category_id' => 'required',
                'course_image' => [
                    'rules' => 'uploaded[course_image]|is_image[course_image]',
                    'errors' => [
                        'uploaded' => 'Image Not Uploaded.',
                        'is_image' => 'The file uploaded is not an image.'
                    ]
                ]
            ]);
            if ($rules) {
                $file = $this->request->getFile('course_image');
                    if ($file->isValid() && !$file->hasMoved()) {
                        $imageName = $file->getRandomName();
                        $file->move('uploads/', $imageName);
                }
=======
<<<<<<< HEAD
            $rules = $this->validate([
                'name' => 'required|is_unique[courses.name]',
                'description' => 'required',
                'category_id' => 'required',
                'course_image' => [
                    'rules' => 'uploaded[course_image]|is_image[course_image]',
                    'errors' => [
                        'uploaded' => 'Image Not Uploaded.',
                        'is_image' => 'The file uploaded is not an image.'
                    ]
                ]
            ]);
            if ($rules) {
                $file = $this->request->getFile('course_image');
                    if ($file->isValid() && !$file->hasMoved()) {
                        $imageName = $file->getRandomName();
                        $file->move('uploads/', $imageName);
                }
=======
<<<<<<< HEAD
            $rules = $this->validate([
                'name' => 'required|is_unique[courses.name]',
                'description' => 'required',
                'category_id' => 'required',
                'course_image' => [
                    'rules' => 'uploaded[course_image]|is_image[course_image]',
                    'errors' => [
                        'uploaded' => 'Image Not Uploaded.',
                        'is_image' => 'The file uploaded is not an image.'
                    ]
                ]
            ]);
            if ($rules) {
                $file = $this->request->getFile('course_image');
                    if ($file->isValid() && !$file->hasMoved()) {
                        $imageName = $file->getRandomName();
                        $file->move('uploads/', $imageName);
                }
=======
<<<<<<< HEAD
            $rules = $this->validate([
                'name' => 'required|is_unique[courses.name]',
                'description' => 'required',
                'category_id' => 'required',
                'course_image' => [
                    'rules' => 'uploaded[course_image]|is_image[course_image]',
                    'errors' => [
                        'uploaded' => 'Image Not Uploaded.',
                        'is_image' => 'The file uploaded is not an image.'
                    ]
                ]
            ]);
            if ($rules) {
                $file = $this->request->getFile('course_image');
                    if ($file->isValid() && !$file->hasMoved()) {
                        $imageName = $file->getRandomName();
                        $file->move('uploads/', $imageName);
                }
=======
<<<<<<< HEAD
            $rules = $this->validate([
                'name' => 'required|is_unique[courses.name]',
                'description' => 'required',
                'category_id' => 'required',
                'course_image' => [
                    'rules' => 'uploaded[course_image]|is_image[course_image]',
                    'errors' => [
                        'uploaded' => 'Image Not Uploaded.',
                        'is_image' => 'The file uploaded is not an image.'
                    ]
                ]
            ]);
            if ($rules) {
                $file = $this->request->getFile('course_image');
                    if ($file->isValid() && !$file->hasMoved()) {
                        $imageName = $file->getRandomName();
                        $file->move('uploads/', $imageName);
                }
=======
            $rules = [
                'name' => 'required|is_unique[courses.name]',
                'description' => 'required',
                'category_id' => 'required',
            ];

            if ($this->validate($rules)) {

                $file = $this->request->getFile('course_image');

                if ($file->isValid() && !$file->hasMoved()) {
                    $imageName = $file->getRandomName();
                    $file->move('uploads/', $imageName);
                }

>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
                $data = [
                    'name' => $this->request->getPost('name'),
                    'description'  => $this->request->getPost('description'),
                    'category_id'  => $this->request->getPost('category_id'),
                    'course_image' => $imageName
                ];
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
                $courseModel->insertCourse($data);
                return redirect()->to(base_url("/admin/Courses/courses"));
            } else {
                $data['categories'] = $courseModel->getCategories1();
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
=======
                $Courses->insertCourse($data);
                return redirect()->to(base_url("/admin/Courses/courses"));
            } else {
                $data['categories'] = $Courses->getCategories1();
>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
                $data['validation'] = $this->validator;
                $this->loadTemplate('courses/addCourse', lang('coruses/new.page_title'), $data);
            }
        }
        $data['categories'] = $courseModel->getCategories1();
        $this->loadTemplate('courses/addCourse', lang('coruses/new.page_title'), $data);
    }
    // edit data 
    public function edit($id)
    {
        $courseModel = new CoursesModel();
        if ($this->request->getMethod() === 'post') {
            $rules = $this->validate([
                'name' => 'required|is_unique[courses.name]',
                'description' => 'required',
                'category_id' => 'required',
                'course_image' => [
                    'rules' => 'uploaded[course_image]|is_image[course_image]',
                    'errors' => [
                        'uploaded' => 'File not uploaded.',
                        'is_image' => 'The file uploaded is not an image.'
                    ]
                ]
            ]);
            if ($rules) {
                $file = $this->request->getFile('course_image');
                if ($file->isValid() && !$file->hasMoved()) {
                    $imageName = $file->getRandomName();
                    $file->move('uploads/', $imageName);
                }
                $courseModel->editCourse($id, [
                    'name' => $this->request->getPost('name'),
                    'description'  => $this->request->getPost('description'),
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
                    //        'language_code' => $this->request->getPost('language_code'),
>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
                    'category_id'  => $this->request->getPost('category_id'),
                    'course_image'  => $imageName,
                ]);
                return redirect()->to(base_url("/admin/Courses/Courses"));
            } else {
                $data['validation'] = $this->validator;
                $data['course'] = $courseModel->getCourse($id);
                $data['categories'] = $courseModel->getCategories1();
                $this->loadTemplate('courses/editCourse', lang('coruses/edit.page_title'), $data);
            }
        }
<<<<<<< HEAD
        $data['course'] = $courseModel->getCourse($id);
        $data['categories'] = $courseModel->getCategories1();
        $this->loadTemplate('courses/editCourse', lang('coruses/edit.page_title'), $data);
=======
<<<<<<< HEAD
        $data['course'] = $courseModel->getCourse($id);
        $data['categories'] = $courseModel->getCategories1();
        $this->loadTemplate('courses/editCourse', lang('coruses/edit.page_title'), $data);
=======
<<<<<<< HEAD
        $data['course'] = $courseModel->getCourse($id);
        $data['categories'] = $courseModel->getCategories1();
        $this->loadTemplate('courses/editCourse', lang('coruses/edit.page_title'), $data);
=======
<<<<<<< HEAD
        $data['course'] = $courseModel->getCourse($id);
        $data['categories'] = $courseModel->getCategories1();
        $this->loadTemplate('courses/editCourse', lang('coruses/edit.page_title'), $data);
=======
<<<<<<< HEAD
        $data['course'] = $courseModel->getCourse($id);
        $data['categories'] = $courseModel->getCategories1();
        $this->loadTemplate('courses/editCourse', lang('coruses/edit.page_title'), $data);
=======

        $Courses = new CoursesModel();
        $data['course'] = $Courses->getCourse($id);
        $data['categories'] = $Courses->getCategories1();
        $this->loadTemplate('courses/editCourse', lang('editCourse.page_title'), $data);
>>>>>>> 767fb37dec7fdb41f27f2d6974965559ff36e987
>>>>>>> 05f30fcf343481dedec11cb1a60750e8928fec14
>>>>>>> eb2f4c5873fe4232e07680d2ad51e36015cf91ec
>>>>>>> dc2f30de12fe4131de165c371c4d162c53171154
>>>>>>> 4bf5a9658a92131768090cb4b5d8cca535be33a0
    }
    // delete course by id 
    public function delete($id)
    {
        $courseModel = new CoursesModel();
        $courseModel->deleteCourse($id);
        return redirect()->to(base_url("/admin/Courses/Courses"));
    }
}
