<?php


namespace App\Controllers\Admin\Courses;

use App\Controllers\Admin\BaseController;
use App\Models\CategoriesModel;
use CodeIgniter\Validation\Rules;

class Categories extends BaseController
{
    public function index()
    {
        $categoryModel = new CategoriesModel();
        $categories = $categoryModel->getCategories();
        $data["categories"] = $categories;
        $this->loadTemplate('categories/categoriesList', lang('categories/list.page_title'), $data);
    }

    public function add()
    {
        $categoryModel = new CategoriesModel();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $rules = $this->validate([
                'name' => 'required|min_length[3]|max_length[30]|alpha_space|is_unique[categories.name]',
                'description'  => 'required|min_length[20]|max_length[255]',
                'category_image' => [
                    'rules' => 'uploaded[category_image]|is_image[category_image]',
                    'errors' => [
                        'uploaded' => 'Image Not Uploaded.',
                        'is_image' => 'The file uploaded is not an image.'
                    ]
                ]
            ]);

            if ($rules) {

                $file = $this->request->getFile('category_image');
                if ($file->isValid() && !$file->hasMoved()) {
                    $imageName = $file->getRandomName();
                    $file->move('uploads/', $imageName);
                }
                $data = [
                    'name' => $this->request->getPost('name'),
                    'description'  => $this->request->getPost('description'),
                    'category_image' => $imageName,
                ];

                $categoryModel->addCategory($data);
                return redirect()->to('/admin/courses/categories');
            } else {
                $data['validation'] = $this->validator;
                $this->loadTemplate('categories/addCategory', lang('categories/new.page_title'), $data);
            }
        } else {
            $this->loadTemplate('categories/addCategory', lang('categories/new.page_title'));
        }
    }

    public function edit($id)
    {
        $categoryModel = new CategoriesModel();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $rules = $this->validate([
                'name' => 'required|min_length[3]|max_length[30]|alpha_space|is_unique[categories.name, id, ' . $id . ']',
                'description'  => 'required|min_length[20]|max_length[255]',
                'category_image' => [
                    'rules' => 'uploaded[category_image]|is_image[category_image]',
                    'errors' => [
                        'uploaded' => 'Image Not Uploaded.',
                        'is_image' => 'The file uploaded is not an image.'
                    ]
                ]
            ]);
            if ($rules) {
                $file = $this->request->getFile('category_image');
                if ($file->isValid() && !$file->hasMoved()) {
                    $imageName = $file->getRandomName();
                    $file->move('uploads/', $imageName);
                }

                $data = [
                    'name' => $this->request->getPost('name'),
                    'description'  => $this->request->getPost('description'),
                    'category_image'  => $imageName,
                ];
                $categoryModel->editCategory(
                    $id,
                    $data
                );
                return redirect()->to(base_url("/admin/courses/categories"));
            } else {

                $data['category'] = $categoryModel->getCategory($id);
                $data['validation'] = $this->validator;
                $this->loadTemplate('categories/editCategory', lang('categories/edit.page_title'), $data);
            }
        } else {
            $category['category'] = $categoryModel->getCategory($id);
            $this->loadTemplate('categories/editCategory', lang('categories/edit.page_title'), $category);
        }
    }

    public function delete($id)
    {
        $categoryModel = new CategoriesModel();
        $categoryModel->deleteCategory($id);
        return redirect()->to('/admin/courses/categories');
    }
}
