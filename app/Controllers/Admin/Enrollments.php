<?php


namespace App\Controllers\Admin\Courses;

use App\Controllers\Admin\BaseController;
use App\Models\EnrollmentsModel;
use CodeIgniter\Validation\Rules;

class Categories extends BaseController
{
    public function index()
    { 
        $this->loadTemplate('categories/categoriesList', lang('categoriesList.page_title'));
    }

  
}
?>