<?php namespace App\Controllers\Admin;

class Home extends BaseController{

    public function index(){

        $this->loadTemplate('home',lang('Home.page_title'));

    }
    
}