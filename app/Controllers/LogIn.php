<?php

namespace App\Controllers;

use App\Controllers\Admin\BaseController;

class LogIn extends BaseController
{
    public function index()
    {
        return view('homepage/login');
    }
    public function login()
    {
        $var = (isset($_POST['is_admin'])) ? 1 : 0;
        if ($this->request->getMethod() === 'post') {
            if ($var == 0) {
                $validation = $this->validate([
                    'email' => [
                        'rules' => 'required|valid_email|is_not_unique[students.email]',
                        'errors' => [
                            'required' => 'Email is required',
                            'valid_email' => 'Enter a valid email addres',
                            'is_not_unique' => 'This email is not registered on our service'
                        ]
                    ],

                    'password' => [
                        'rules' => 'required|min_length[5]|max_length[12]',
                        'errors' => [
                            'required' => 'password is required',
                            'min_length' => 'password must have atleast 5 characters in length',
                            'max_length' => 'password must not have more than 12  characters in length '
                        ]
                    ]
                ]);
                if (!$validation) {
                    return view('homepage/login', ['validation' => $this->validator]);
                } else {
                    return view('homepage/homePage');
                }
            } else {
                $validation = $this->validate([
                    'email' => [
                        'rules' => 'required|valid_email|is_not_unique[admins.email]',
                        'errors' => [
                            'required' => 'Email is required',
                            'valid_email' => 'Enter a valid email addres',
                            'is_not_unique' => 'This email is not registered on our service'
                        ]
                    ],

                    'password' => [
                        'rules' => 'required|min_length[5]|max_length[12]',
                        'errors' => [
                            'required' => 'password is required',
                            'min_length' => 'password must have atleast 5 characters in length',
                            'max_length' => 'password must not have more than 12  characters in length '
                        ]
                    ]
                ]);

                if (!$validation) {
                    return view('homepage/login', ['validation' => $this->validator]);
                } else {
                    echo 'admin dashboard ';
                }
            }
        } else {
            return view('homepage/login');
        }
    }
}
