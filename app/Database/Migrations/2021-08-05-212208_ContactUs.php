<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ContactUs extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'       => [
				'type'       => 'int',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'icon_code'       => [
				'type'       => 'VARCHAR',
				'constraint' => 200,
				
			],
			'link' => [
				'type' => 'VARCHAR',
				'constraint' => 225,
			],
			'title' => [
				'type' => 'VARCHAR',
				'constraint' => 70,],

		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('contactUs');
	}

	public function down()
	{
		$this->forge->dropTable('contactUs');
	}
}


