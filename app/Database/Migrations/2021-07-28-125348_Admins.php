<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Admins extends Migration
{
	public function up()
	{

		//create admins tabe with (id , email , password , is active , added by , modified by , created at and modified at) column
		$this->forge->addField([
			'id'=>[
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'email' => [
				'type' => 'VARCHAR',
				'constraint' => 225,
			],
			'user_name' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
			],
			'password' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'is_active' => [
				'type' => 'BOOLEAN',
				'default' => 1,
			],
	]);

	$this->forge->addKey('id', true);
	$this->forge->createTable('admins');

	}

	public function down()
	{
		$this->forge->dropTable('admins');
	}
}
