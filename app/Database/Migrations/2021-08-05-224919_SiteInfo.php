<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SiteInfo extends Migration
{
	public function up()
	{
	$this->forge->addField([
		'id'       => [
			'type'       => 'int',
			'constraint' => 11,
			'auto_increment' => true,
		],
		'name' => [
			'type' => 'VARCHAR',
			'constraint' => 35,
		],
		'icon_path'       => [
			'type'       => 'VARCHAR',
			'constraint' => 200,
			
		],
		
		'logo_path' => [
			'type' => 'VARCHAR',
			'constraint' => 200,
		],

	]);
	$this->forge->addKey('id', true);
	$this->forge->createTable('siteInfo');
}

	public function down()
	{
		$this->forge->dropTable('siteInfo');
	}
}
