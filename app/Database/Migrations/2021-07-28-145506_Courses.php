<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Courses extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 11,
				'auto_increment' => true,
			],
			'name'       => [
				'type'       => 'VARCHAR',
				'constraint' => 30,
			],
			'course_image'       => [
				'type'       => 'VARCHAR',
				'constraint' => 255,
			],
			'description' => [
				'type' => 'VARCHAR',
				'constraint'     => 500,
			],
			'language_code'          => [
				'type'           => 'VARCHAR',
				'constraint'     => 2,

			],
			'category_id'       => [
				'type'       => 'INT',
				'constraint' => 11,
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('courses');
	}

	public function down()
	{
		$this->forge->dropTable('courses');
	}
}