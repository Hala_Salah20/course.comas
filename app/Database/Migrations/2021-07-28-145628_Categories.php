<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Categories extends Migration
{
	public function up()
	{
			$this->forge->addField([
				'id'          => [
						'type'           => 'INT',
						'constraint'     => 11,
						'auto_increment' => true,
				],
				'name'       => [
						'type'       => 'VARCHAR',
						'constraint' => 30,
				],
				'description' => [
						'type' => 'TEXT',
						'constraint' => 500,
				],
				'language_code' => [
						'type' => 'VARCHAR',
						'constraint' => 2,
						'default' => 'en'
				],
				'category_image'       => [
					'type'       => 'VARCHAR',
					'constraint' => 255,
				],
				
		]);
			$this->forge->addKey('id', true);
			$this->forge->createTable('categories');
	}

	public function down()
	{
		$this->forge->dropTable('categories');
	}
}
