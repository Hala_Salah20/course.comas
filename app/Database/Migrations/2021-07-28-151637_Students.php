<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Students extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'=>[
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'email' => [
				'type' => 'VARCHAR',
				'constraint' => 225,
			],
			'user_name' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
			],
			'password' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'is_active' => [
				'type' => 'BOOLEAN',
				'default' => 1,
			],
			'ban' => [
				'type' => 'BOOLEAN',
				'default' => 0,
			],
	]);
	
	$this->forge->addKey('id', true);
	$this->forge->createTable('students');
}
	

	public function down()
	{
		$this->forge->dropTable('students');		//
	}

}