<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Enrollments extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'course_id'=>[
				'type' => 'INT',
				'constraint' => 11,
			],
			'student_id'=>[
				'type' => 'INT',
				'constraint' => 11,
			],
			'status' => [
				'type' => 'BOOLEAN',
				'default' => 0,
			],
	]);

	$this->forge->createTable('enrollments');

	}

	public function down()
	{
		$this->forge->dropTable('enrollments');
	}
}
