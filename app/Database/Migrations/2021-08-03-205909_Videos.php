<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Videos extends Migration
{
	public function up()
	{

		//create website info tabe with (name, logopath, iconpath, modified by and modified at) columns
		$this->forge->addField([

			'id'=>[
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'path' => [
				'type' => 'VARCHAR',
				'constraint' => 225,
			],
			'upload_video' => [
				'type' => 'VARCHAR',
				'constraint' => 225,
			],
			'title' => [
				'type' => 'VARCHAR',
				'constraint' => 70,
			],
			'course_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],

		]);

	$this->forge->addKey('id', true);
	$this->forge->createTable('videos');
	}

	public function down()
	{
		$this->forge->dropTable('videos');
	}
	}